# O
  - Unit Test : Unit testing refers to the inspection and verification of the smallest testable unit in software.It is divided into four parts, namely  Test description, Given(prepare),When(execute) and Then(compare).
  - TDD : TDD requires writing the test code before writing the code for a certain function, and then writing only the functional code that makes the test pass. 
  -  Advantages of TDD : This helps to write concise, usable and high-quality code and speeds up the development process.
  - Step of TDD : 1、RED(write a test that fails) 2、GREEN (make the code work) 3、REFACTOR(eliminate redundancy).
  

# R
  - Innovative !
# I
  - I think today's lesson taught me to write concise, usable and high-quality code and speeds up the development process.The biggest significance is that I will Verify and standardize development through TDD.
# D
  - Use TDD for daily development.
  - Deepen your study of TDD and learn about other related concepts.

