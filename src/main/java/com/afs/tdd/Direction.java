package com.afs.tdd;

public enum Direction {
    WEST,
    SOUTH,
    EAST,
    NORTH
}
