package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_north_when_executeCommandAndReportLocation_given_command_move_location_0_0_north() {
        //Given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.M;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_1_0_east_when_executeCommandAndReportLocation_given_command_move_location_0_0_east() {
        //Given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.M;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_1_west_when_executeCommandAndReportLocation_given_command_move_location_1_1_west() {
        //Given
        Location location = new Location(1, 1, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.M;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_1_0_south_when_executeCommandAndReportLocation_given_command_move_location_1_1_south() {
        //Given
        Location location = new Location(1, 1, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.M;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_east_when_executeCommandAndReportLocation_given_command_left_location_0_0_south() {
        //Given
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.L;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_north_when_executeCommandAndReportLocation_given_command_left_location_0_0_east() {
        //Given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.L;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_west_when_executeCommandAndReportLocation_given_command_left_location_0_0_north() {
        //Given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.L;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_south_when_executeCommandAndReportLocation_given_command_left_location_0_0_west() {
        //Given
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.L;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_north_when_executeCommandAndReportLocation_given_command_right_location_0_0_west() {
        //Given
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.R;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_east_when_executeCommandAndReportLocation_given_command_right_location_0_0_north() {
        //Given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.R;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_south_when_executeCommandAndReportLocation_given_command_right_location_0_0_east() {
        //Given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.R;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_west_when_executeCommandAndReportLocation_given_command_right_location_0_0_south() {
        //Given
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);
        Command command = Command.R;
        //When
        Location locationAfterMove = marsRover.executeCommandAndReportLocation(command);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_1_west_when_executeMultipleCommandAndReportLocation_given_command_MRMLL_location_0_0_north() {
        //Given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);
        List<Command> commandList = new ArrayList<>();
        commandList.add(Command.M);
        commandList.add(Command.R);
        commandList.add(Command.M);
        commandList.add(Command.L);
        commandList.add(Command.L);
        //When
        Location locationAfterMove = marsRover.executeMultipleCommandAndReportLocation(commandList);
        //Then
        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,locationAfterMove.getDirection());
    }
}
