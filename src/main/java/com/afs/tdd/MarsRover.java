package com.afs.tdd;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommandAndReportLocation(Command command) {
        if (command == Command.M) {
            executeCommandOfMove();
        }
        if (command == Command.L) {
            executeCommandOfLeft();
        }
        if (command == Command.R) {
            executeCommandOfRight();
        }
        return this.location;
    }

    public void executeCommandOfMove() {
        if (this.location.getDirection() == Direction.NORTH) {
            this.location.setCoordinateY(this.location.getCoordinateY() + 1);
        }
        if (this.location.getDirection() == Direction.EAST) {
            this.location.setCoordinateX(this.location.getCoordinateX() + 1);
        }
        if (this.location.getDirection() == Direction.WEST) {
            this.location.setCoordinateX(this.location.getCoordinateX() - 1);
        }
        if (this.location.getDirection() == Direction.SOUTH) {
            this.location.setCoordinateY(this.location.getCoordinateY() - 1);
        }

    }

    public void executeCommandOfLeft() {
        if (this.location.getDirection() == Direction.SOUTH) {
            this.location.setDirection(Direction.EAST);
        } else if (this.location.getDirection() == Direction.EAST) {
            this.location.setDirection(Direction.NORTH);
        } else if (this.location.getDirection() == Direction.NORTH) {
            this.location.setDirection(Direction.WEST);
        } else {
            this.location.setDirection(Direction.SOUTH);
        }
    }

    public void executeCommandOfRight() {
        if (this.location.getDirection() == Direction.WEST) {
            this.location.setDirection(Direction.NORTH);
        } else if (this.location.getDirection() == Direction.NORTH) {
            this.location.setDirection(Direction.EAST);
        } else if (this.location.getDirection() == Direction.EAST) {
            this.location.setDirection(Direction.SOUTH);
        } else {
            this.location.setDirection(Direction.WEST);
        }
    }

    public Location executeMultipleCommandAndReportLocation(List<Command> commandList) {
        commandList.forEach(this::executeCommandAndReportLocation);
        return this.location;
    }
}
